package martsekha.igor.homework2;

import android.app.FragmentManager;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;

public class MainActivity extends AppCompatActivity {
    RadioButton redRadioBtn;
    RadioButton greenRadioBtn;
    RadioButton blueRadioBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initUI();

        getFragmentManager().addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
            @Override
            public void onBackStackChanged() {
                clearAllRadioButtons();

                ColorFragment fragment = (ColorFragment)getFragmentManager().findFragmentById(R.id.fragment_container);
                if(fragment == null)
                    return;

                switch (fragment.getCustomTag()) {
                    case R.id.redRadioBtn:
                        redRadioBtn.setChecked(true);
                        break;
                    case R.id.greenRadioBtn:
                        greenRadioBtn.setChecked(true);
                        break;
                    case R.id.blueRadioBtn:
                        blueRadioBtn.setChecked(true);
                        break;
                    default:
                        clearAllRadioButtons();
                        break;
                }
            }
        });
    }

    private void initUI() {
        View.OnClickListener onClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ColorFragment fragment = new ColorFragment();
                switch (v.getId()) {
                    case R.id.redRadioBtn:
                        fragment.setColor(Color.RED);
                        fragment.setCustomTag(R.id.redRadioBtn);
                        break;
                    case R.id.greenRadioBtn:
                        fragment.setColor(Color.GREEN);
                        fragment.setCustomTag(R.id.greenRadioBtn);
                        break;
                    case R.id.blueRadioBtn:
                        fragment.setColor(Color.BLUE);
                        fragment.setCustomTag(R.id.blueRadioBtn);
                        break;
                }

                getFragmentManager()
                        .beginTransaction()
                        .replace(R.id.fragment_container, fragment)
                        .addToBackStack(null)
                        .commit();
            }
        };

        redRadioBtn = (RadioButton) findViewById(R.id.redRadioBtn);
        redRadioBtn.setOnClickListener(onClickListener);
        greenRadioBtn = (RadioButton) findViewById(R.id.greenRadioBtn);
        greenRadioBtn.setOnClickListener(onClickListener);
        blueRadioBtn = (RadioButton) findViewById(R.id.blueRadioBtn);
        blueRadioBtn.setOnClickListener(onClickListener);
    }

    public void clearAllRadioButtons() {
        redRadioBtn.setChecked(false);
        greenRadioBtn.setChecked(false);
        blueRadioBtn.setChecked(false);
    }

    public void onResumeFragmentListener(int customTag) {
//        clearAllRadioButtons();
//        switch (customTag) {
//            case R.id.redRadioBtn:
//                redRadioBtn.setChecked(true);
//                break;
//            case R.id.greenRadioBtn:
//                greenRadioBtn.setChecked(true);
//                break;
//            case R.id.blueRadioBtn:
//                blueRadioBtn.setChecked(true);
//                break;
//            default:
//                clearAllRadioButtons();
//                break;
//        }
    }
}
