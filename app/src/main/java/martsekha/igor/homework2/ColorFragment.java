package martsekha.igor.homework2;

import android.app.Fragment;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by adventis on 5/14/16.
 */
public class ColorFragment extends Fragment {

    public int getColor() {
        return color;
    }

    public void setColor(int currentColor) {
        this.color = currentColor;
    }

    private int color = Color.WHITE;

    public int getCustomTag() {
        return customTag;
    }

    public void setCustomTag(int customTag) {
        this.customTag = customTag;
    }

    private int customTag;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_layout, container,false);
        view.setBackgroundColor(this.color);
        view.setAlpha(0.5f);
        return view;
    }
}
